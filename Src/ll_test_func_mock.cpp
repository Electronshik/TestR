#include "ll_test_func.h"
#include "CppUTestExt/MockSupport.h"

#include <iostream>

void ll_test_func(int x)
{
	std::cout << "ll_test_func " << x << std::endl;
	mock().actualCall(__func__).withParameter("x", x);
}
