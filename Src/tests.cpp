#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

#include "MemoryTesteR.h"
#include "BUFF_MemoryWriteR.h"

#include "test_funcs.h"

MemoryParams_t MemoryParams =
{
	.BeginAddr = 0x60000000,
	.FullSize = 512,
	.ErasedVal = 0xff,
	.PageSize = 256
};

BUFF_IMemoryWriteR BUFF_IMemoryWriter = BUFF_IMemoryWriteR(MemoryParams);
MemoryTesteR<BUFF_IMemoryWriteR> MemoryTester(BUFF_IMemoryWriter);

TEST_GROUP(FirstTestGroup)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};

TEST(FirstTestGroup, FirstTest)
{
	CHECK(MemoryTester.TestPageReadEraseWrite(0x60000000 + MemoryParams.FullSize) == ErrorCode::ERROR);
	CHECK(MemoryTester.TestPageReadEraseWrite(0x60000000) == ErrorCode::OK);
	CHECK(MemoryTester.TestPageReadEraseWrite(0x60000000 + 256) == ErrorCode::OK);
	// FAIL("Fail me!");
}

TEST(FirstTestGroup, SecondTest)
{
	// STRCMP_EQUAL("hello", "world");
	// LONGS_EQUAL(1, 2);
	// CHECK(false);
}

TEST_GROUP(MockTestGroup)
{
	void setup()
	{
	}

	void teardown()
	{
		mock().clear();
	}
};

TEST(MockTestGroup, FirstTest)
{
	// arrange
	mock().expectOneCall("ll_test_func").withParameter("x", 8);

	// act
	test_func(8);

	// assert
	mock().checkExpectations();
	// CHECK_EQUAL(5.0f, hypotenuse);
}
