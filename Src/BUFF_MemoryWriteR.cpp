#include "BUFF_MemoryWriteR.h"

bool BUFF_IMemoryWriteR::CheckIfAddrAvail(uint32_t addr, uint32_t len)
{
	if((addr < MemoryParams.BeginAddr) ||
		((addr + len) > (MemoryParams.BeginAddr + MemoryParams.FullSize)))
	{
		return false;
	}
	return true;
}

void BUFF_IMemoryWriteR::Erase(uint32_t addr, uint32_t len)
{
	if(!CheckIfAddrAvail(addr, len))
		return;

	addr = addr - MemoryParams.BeginAddr;

	uint32_t actualBeginAddr, actualEndAddr, actualEraseLen;
	if(((addr - MemoryParams.BeginAddr) % MemoryParams.PageSize) != 0)
	{

		uint32_t pageNumFromBegin = (addr - MemoryParams.BeginAddr) / MemoryParams.PageSize;
		uint32_t offsetFromPageBegin = (pageNumFromBegin * MemoryParams.PageSize);
		actualBeginAddr = addr - offsetFromPageBegin;
		actualEraseLen = len + offsetFromPageBegin;
	}
	else
	{
		actualBeginAddr = addr;
		actualEraseLen = len;
	}

	if((actualEraseLen % MemoryParams.PageSize) == 0)
	{
		actualEndAddr = actualBeginAddr + actualEraseLen;
	}
	else
	{
		uint32_t wholePagesNumToErase = 1 + (actualBeginAddr - MemoryParams.BeginAddr + actualEraseLen) / MemoryParams.PageSize;
		uint32_t wholePagesEndEraseAddr = wholePagesNumToErase * MemoryParams.PageSize;
		actualEndAddr = actualBeginAddr + wholePagesEndEraseAddr;
		actualEraseLen = actualEndAddr - actualBeginAddr;
	}

	for(uint32_t i = actualBeginAddr; i < actualEndAddr; i++)
		memArray.at(i) = MemoryParams.ErasedVal;

	std::cout << "BUFF_IMemoryWriteR::Erased " << len << " bytes" << std::endl;
}

void BUFF_IMemoryWriteR::Write(uint32_t addr, dType* buff, uint32_t len)
{
	if(!CheckIfAddrAvail(addr, len))
		return;

	addr = addr - MemoryParams.BeginAddr;

	uint32_t endAddr = addr + len;
	for(uint32_t i = addr; i < endAddr; i++)
	{
		memArray.at(i) &= buff[i-addr];
	}

	std::cout << "BUFF_IMemoryWriteR::Writen " << len << " bytes" << std::endl;
}

void BUFF_IMemoryWriteR::Read(uint32_t addr, dType* buff, uint32_t len)
{
	if(!CheckIfAddrAvail(addr, len))
		return;

	addr = addr - MemoryParams.BeginAddr;

	uint32_t endAddr = addr + len;
	for(uint32_t i = addr; i < endAddr; i++)
		buff[i-addr] = memArray.at(i);

	std::cout << "BUFF_IMemoryWriteR::Readed " << len << " bytes" << std::endl;
}
