#pragma once
#include <iostream>

enum class ErrorCode
{
	OK,
	ERROR
};

typedef struct
{
	uint32_t BeginAddr;
	uint32_t FullSize;
	uint32_t ErasedVal;
	uint32_t PageSize;
} MemoryParams_t;

template<typename T>
class IMemoryWriteR
{
	public:
		using dType = T;
		MemoryParams_t MemoryParams;
		dType ErasedValue;
		virtual void Erase(uint32_t addr, uint32_t len) = 0;
		virtual void Write(uint32_t addr, dType* buff, uint32_t len) = 0;
		virtual void Read(uint32_t addr, dType* buff, uint32_t len) = 0;
		virtual ~IMemoryWriteR() {}

	protected:
		IMemoryWriteR(MemoryParams_t& MemoryParams) : MemoryParams(MemoryParams), ErasedValue((dType)MemoryParams.ErasedVal) {}
};

template<class memIFace>
class MemoryTesteR
{
	public:
		using dType = typename memIFace::dType;
		MemoryTesteR(memIFace& MemoryWriter) : MemoryWriter(MemoryWriter) {}
		ErrorCode TestPageReadEraseWrite(uint32_t addr);

	private:
		memIFace& MemoryWriter;
};

template<class memIFace>
ErrorCode MemoryTesteR<memIFace>::TestPageReadEraseWrite(uint32_t addr)
{
	uint32_t endPageAddr = MemoryWriter.MemoryParams.BeginAddr + MemoryWriter.MemoryParams.FullSize - MemoryWriter.MemoryParams.PageSize;
	if((addr < MemoryWriter.MemoryParams.BeginAddr) ||
		(addr > (endPageAddr)))
	{
		std::cout << "TestPageReadEraseWrite Invalid Parameters" << std::endl;
		return ErrorCode::ERROR;
	}

	const uint32_t pageSize = MemoryWriter.MemoryParams.PageSize;
	dType readBuff[pageSize];
	dType buff[pageSize];

	MemoryWriter.Read(addr, readBuff, pageSize);
	MemoryWriter.Erase(addr, pageSize);
	MemoryWriter.Read(addr, buff, pageSize);

	bool success = true;
	for(auto& el : buff)
	{
		if(el != MemoryWriter.ErasedValue)
		{
			success = false;
			break;
		}	
	}

	if(success)
	{
		MemoryWriter.Write(addr, readBuff, pageSize);
		MemoryWriter.Read(addr, buff, pageSize);

		for(uint32_t i = 0; i < pageSize; i++)
		{
			if(buff[i] != readBuff[i])
			{
				success = false;
				break;
			}
		}
	}

	if(success)
	{
		std::cout << "TestPageReadEraseWrite Success: True" << std::endl << std::endl;
		return ErrorCode::OK;
	}
	else
	{
		std::cout << "TestPageReadEraseWrite Success: False" << std::endl << std::endl;
		return ErrorCode::ERROR;
	}
};
