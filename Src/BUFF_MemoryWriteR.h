#pragma once
#include "MemoryTesteR.h"
#include <vector>

class BUFF_IMemoryWriteR : public IMemoryWriteR<uint8_t>
{
	public:
		// using dType = IMemoryWriteR<uint8_t>::dType;
		BUFF_IMemoryWriteR(MemoryParams_t& MemoryParams) : IMemoryWriteR<dType>(MemoryParams)
		{
			memArray.resize(MemoryParams.FullSize);
		}
		virtual void Erase(uint32_t addr, uint32_t len) override;
		virtual void Write(uint32_t addr, dType* buff, uint32_t len) override;
		virtual void Read(uint32_t addr, dType* buff, uint32_t len) override;

	private:
		std::vector<dType> memArray;
		bool CheckIfAddrAvail(uint32_t addr, uint32_t len);
};
